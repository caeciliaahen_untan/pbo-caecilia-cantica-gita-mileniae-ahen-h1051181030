class Peserta{
    private int nomorpeserta;
    private String namapeserta;
    private String idline;
    private String ingamename;
    private String domisilipeserta;
    private String kategoriturnamen;
    private String karaktergame;

    public Peserta(int nomor, String nama, String id, String ign, String     domisili, String kategori, String karakter){
        this.nomorpeserta=nomor;
        this.namapeserta=nama;
        this.idline=id;
        this.ingamename=ign;
        this.domisilipeserta=domisili;
        this.kategoriturnamen=kategori;
        this.karaktergame=karakter;
    }

    public void tampilData(){
        System.out.println("Nomor peserta ["+this.nomorpeserta+"]");
        System.out.println("Nama peserta \t \t \t: "+this.namapeserta);
        System.out.println("ID Line \t \t \t: "+this.idline);
        System.out.println("In game name \t \t \t: "+this.ingamename);
        System.out.println("Domisili \t \t \t: "+this.domisilipeserta);
        System.out.println("Kategori \t \t \t: "+this.kategoriturnamen);
        System.out.println("Karakter yang dimainkan \t: "+this.karaktergame);
        System.out.println("");
    }
}

public class dataPeserta{
    public static void main(String[]args){
        System.out.println("D  A  T  A    P  E  S  E  R  T  A \n");

        Peserta p1=new Peserta(1001, "Giveld Alifia", "@giveldalifia89", "BJ-031", "Pekanbaru", "TTL", "Puppeteer Costantine");
            p1.tampilData();

        Peserta p2=new Peserta(1002, "Renaldi Muller", "@renaldimuller", "BJ-006", "Bogor", "TTL", "Ariel");
            p2.tampilData();

        Peserta p3=new Peserta(1003, "Raflesia Andhini", "@rflsya_", "Dhin", "Pontianak", "TTL", "Dark Muse");
            p3.tampilData();

        Peserta p4=new Peserta(2001, "Bonar Wahyudi", "@bonar_wahyudi", "Bonar-kun CR-7", "Medan", "Bankrupt", "Dark Ariel");
            p4.tampilData();

        Peserta p5=new Peserta(2002, "Hendro Susilo", "@hendrosusilo96", "Aldy Aulia", "Makassar", "Bankrupt", "Mix Mirage");
            p5.tampilData();

        Peserta p6=new Peserta(2003, "Yadi", "@yadi081225575877", "[Far]Yadi", "Sumedang", "Bankrupt", "Valentine Chihiro");
            p6.tampilData();
    }
}